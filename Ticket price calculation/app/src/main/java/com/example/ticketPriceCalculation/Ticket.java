package com.example.ticketPriceCalculation;

public class Ticket {
    public int ticketPrice;
    public int childTicketPrice;
    private int pensionerTicketPrice;

    private String travelTime;
    private String departureTime;
    private String departurePoint;
    private String arrivalPoint;

    private int childDiscount;
    private int pensionerDiscount;


    public Ticket(int childDiscount, int pensionerDiscount, String travelTime, String departurePoint, String departureTime, String arrivalPoint){
        this.childDiscount = childDiscount;
        this.pensionerDiscount = pensionerDiscount;
        this.travelTime = travelTime;
        this.departurePoint = departurePoint;
        this.departureTime = departureTime;
        this.arrivalPoint = arrivalPoint;
    }

    public int getTicketPrice(){
        return ticketPrice;
    }
    public int getChildTicketPrice(){
        return ticketPrice * (100-childDiscount) / 100;
    }
    public int getPensionerTicketPrice(){
        return ticketPrice * (100-pensionerDiscount) / 100;
    }

    public void setTicketPrice(int ticketPrice){
        this.ticketPrice = ticketPrice;
    }
    public void setChildTicketPrice(int childTicketPrice){
        this.childTicketPrice = childTicketPrice;
    }
    public void setPensionerTicketPrice(int pensionerTicketPrice){
        this.pensionerTicketPrice = pensionerTicketPrice;
    }
}

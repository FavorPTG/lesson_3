package com.example.ticketPriceCalculation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView resultText;
    EditText ticketPriceText;
    EditText childDiscountText;
    EditText pensionerDiscountText;
    EditText numberBaseTicketsText;
    EditText numberChildTicketsText;
    EditText numberPensionerTicketsText;
    Button button;
    int result = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultText = findViewById(R.id.resultText);
        ticketPriceText = findViewById(R.id.ticketPrice);
        childDiscountText = findViewById(R.id.childDiscount);
        pensionerDiscountText = findViewById(R.id.pensionerDiscount);
        numberBaseTicketsText = findViewById(R.id.numberBaseTickets);
        numberChildTicketsText = findViewById(R.id.numberChildTickets);
        numberPensionerTicketsText = findViewById(R.id.numberPensionerTickets);
        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ticketPrice = Integer.parseInt(ticketPriceText.getText().toString());
                int childDiscount = Integer.parseInt(childDiscountText.getText().toString());
                int pensionerDiscount = Integer.parseInt(pensionerDiscountText.getText().toString());
                int numberBaseTickets = Integer.parseInt(numberBaseTicketsText.getText().toString());
                int numberChildTickets = Integer.parseInt(numberChildTicketsText.getText().toString());
                int numberPensionerTickets = Integer.parseInt(numberPensionerTicketsText.getText().toString());

                Ticket ticket = new Ticket(childDiscount, pensionerDiscount, "4 часа 30 минут", "Горно-алтайск", "7:00 1 июня", "Артыбаш");

                ticket.setTicketPrice(ticketPrice);

                for(int i=0; i < numberBaseTickets; i++){
                    result += ticket.getTicketPrice();
                }
                for(int i=0; i < numberChildTickets; i++){
                    result += ticket.getChildTicketPrice();
                }
                for(int i=0; i < numberPensionerTickets; i++){
                    result += ticket.getPensionerTicketPrice();
                }

                resultText.setText("Результат:\n" + result + " монет");
                result = 0;
            }
        });
    }
}